const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Department = require('./models/department');

const authenticate = require('./middlewares/authenticate');

var response = require('./service/respondent');


const departmentRouter = express.Router();

departmentRouter.use(bodyParser.json());

departmentRouter.route('/')
    .get( (req, res, next) =>{
        Department.find({}, (err, dep) => {
            if(err){
                response.errorResponse(res, 500, 'ServerError', 'Contact Admin');
            }
            if(dep){
                response.dataResponce(res, 200, dep, 'Successfully listed the departments');
            }
        })
    })
    .post(authenticate.verifyUser, authenticate.verifyAdmmin, (req, res, next) =>{
        var newDep = new Department();

        newDep = req.body;
        newDep.save((err) => {
            if(err){
                response.errorResponse(res, 500, 'ServerError', 'Contact Admin');
            }else{
                response.dataResponce(res, 200, newDep, 'Successfully listed the departments');
            }
        })
    });