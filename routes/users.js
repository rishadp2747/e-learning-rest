var express = require('express');
const bodyParser = require('body-parser');

var User = require('../models/user');
var passport = require('passport');
var authenticate = require('./middlewares/authenticate');

var router = express.Router();

router.use(bodyParser.json());

router.post('/signup', (req, res, next) => {
  User.register( new User({username: req.body.username, name : req.body.name}), req.body.password, (err, user) =>{
    if(err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'application/json');
      res.json({err : err});
    }
    else {
      passport.authenticate('local')(req,res, () => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({success : true, status: 'Registration Successful!'});
      });
    }

  });
});

router.post('/login', passport.authenticate('local'), (req, res) => {
  var token = authenticate.getToken({__id: req.user._id});
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json({success : true, status: 'Successfuly loged in !', token : token});
})


module.exports = router;
