const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Subject = require('./models/subject');

const authenticate = require('./middlewares/authenticate');

var response = require('./service/respondent');


const subjectRouter = express.Router();

subjectRouter.use(bodyParser.json());

subjectRouter.route('/')
    .get( (req, res, next) =>{
        Subject.find({}, (err, Sub) => {
            if(err){
                response.errorResponse(res, 500, 'ServerError', 'Contact Admin');
            }
            if(Sub){
                response.dataResponce(res, 200, Sub, 'Successfully listed the Subjects');
            }
        })
    })
    .post(authenticate.verifyUser, authenticate.verifyAdmmin, (req, res, next) =>{
        var newSub = new Subject();
        newSub = req.body;
        newSub.save((err) => {
            if(err){
                response.errorResponse(res, 500, 'ServerError', 'Contact Admin');
            }else{
                response.dataResponce(res, 200, newSub, 'Successfully listed the Subjects');
            }
        })
    });

subjectRouter.route('/:subId')
    .get( authenticate.verifyUser, (req, res, next) =>{
        Subject.findById({'_id' : req.params.subId}, (err, sub) =>{
            if(err){
                response.errorResponse(res, 500, 'ServerError', 'Contact Admin');
            }
            
            if(sub){
                response.dataResponce(res, 200, sub, 'Successfully listed the Subject');
            }
        })
    })