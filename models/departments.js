var mongoose = require('mongoose');
var Schema = mongoose.Schema;


const depSchema = new Schema({
    name : {
        required : true,
        type : String,
    },
});

depSchema.plugin(passportLocalMongoose);

exports.module = mongoose.model('Department', depSchema);