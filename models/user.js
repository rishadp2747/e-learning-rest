var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');


const userSchema = new Schema({
    name : {
        required : true,
        type : String,
    },

    faculty : {
        type : Boolean,
        default : false,
    },

    admin : {
        type : Boolean,
        default : false,
    }

});

userSchema.plugin(passportLocalMongoose);

exports.module = mongoose.model('User', userSchema);