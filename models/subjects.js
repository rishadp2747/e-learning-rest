var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const videoSchema = new Schema({
    title : {
        required : true,
        type : String
    },
    description : {
        required : true,
        type : String
    },
    url: {
        required :true,
        type: String
    },
    author : {
        required : true,
        type : Schema.Types.ObjectId,
        ref : 'User'
    },
    language : {
        required : true,
        type : String
    }
});

const subjectSchema = new Schema({
    name : {
        required : true,
        type : String,
    },
    department : {
        required : true,
        type : Schema.Types.ObjectId,
        ref : 'Department'
    },
    videos : {
        type : [videoSchema]
    }
});

subjectSchema.plugin(passportLocalMongoose);

exports.module = mongoose.model('subject', subjectSchema);